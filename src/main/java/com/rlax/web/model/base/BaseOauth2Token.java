package com.rlax.web.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseOauth2Token<M extends BaseOauth2Token<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}

	public java.lang.Long getId() {
		return get("id");
	}

	public void setToken(java.lang.String token) {
		set("token", token);
	}

	public java.lang.String getToken() {
		return get("token");
	}

	public void setExpires(java.lang.Long expires) {
		set("expires", expires);
	}

	public java.lang.Long getExpires() {
		return get("expires");
	}

	public void setClientId(java.lang.String clientId) {
		set("client_id", clientId);
	}

	public java.lang.String getClientId() {
		return get("client_id");
	}

	public void setUid(java.lang.String uid) {
		set("uid", uid);
	}

	public java.lang.String getUid() {
		return get("uid");
	}

}
