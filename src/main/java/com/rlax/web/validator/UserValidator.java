package com.rlax.web.validator;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;
import com.rlax.web.model.User;

/**
 * 用户校验
 * @author Rlax
 *
 */
public class UserValidator extends Validator  {

	@Override
	protected void validate(Controller c) {
		validateRequiredString("user.name", "nameMsg", "请输入用户名");
		validateRequiredString("user.pwd", "pwdMsg", "请输入密码");
		validateRequiredString("user.email", "emailMsg", "请输入邮箱");
		validateRequiredString("user.phone", "phoneMsg", "请输入电话");
		
		if (StrKit.notBlank(c.getPara("user.name"))) {
			if (User.dao.findByName(c.getPara("user.name")) != null) {
				addError("nameMsg", "用户名已存在");
			}
		}
	}

	@Override
	protected void handleError(Controller c) {
		c.keepPara();
		c.render("add.html");		
	}

}
