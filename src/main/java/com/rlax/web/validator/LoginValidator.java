package com.rlax.web.validator;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;
import com.rlax.web.model.User;

/**
 * 登录校验
 * @author Rlax
 *
 */
public class LoginValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRequiredString("loginName", "loginNameMsg", "请输入用户名");
		validateRequiredString("password", "passwordMsg", "请输入密码");
		validateCaptcha("capval", "capvalMsg", "验证码不正确");
	}

	@Override
	protected void handleError(Controller c) {
		c.keepPara("loginName");
		c.keepPara("password");
		c.render("login.html");
	}

}
