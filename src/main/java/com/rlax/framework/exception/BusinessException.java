package com.rlax.framework.exception;

/**
 * 业务异常
 * @author Rlax
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 2398740334943163701L;

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

}
