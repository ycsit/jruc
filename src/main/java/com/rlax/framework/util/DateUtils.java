package com.rlax.framework.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.ext2.kit.DateTimeKit;

public class DateUtils {

	/**
	 * 如果填充ORACLE DATE类型字段，请使用该方法
	 * @return
	 */
	public static java.sql.Timestamp getOracleDate () {
		return new java.sql.Timestamp(new java.util.Date().getTime());
	}
	
	/**
	 * 时间转换
	 * @param style， DateKit.FULL_DATE_24HR_STYLE etc
	 * @param dateString
	 * @return
	 */
	public static Date dateStringToDate(String dateString, String style){
		SimpleDateFormat sdf=new SimpleDateFormat(style);
		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {}  
		return date;
	}
	
	/**
	 * 获取当前时间：2016-04-21 15:56:24
	 * @return
	 */
	public static String getDateTime() {
		return DateTimeKit.formatDateToFULL24HRStyle("-", new Date());
	}
	
	/**
	 * 获取当前日期： 默认格式 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getDateTime(String style) {
		return getDateTime(style, new Date());
	}
	
	/**
	 * 获取指定日期
	 * @return
	 */
	public static String getDateTime(String style, Date date) {
		return DateTimeKit.formatDateToStyle(style, date);
	}
}
