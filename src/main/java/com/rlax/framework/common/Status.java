package com.rlax.framework.common;

/**
 * 状态常量
 * @author Rlax
 *
 */
public class Status {

	/**
	 * 通用 0-未启用 1-通用
	 */
	public static final String COMMON_USE = "1";
	public static final String COMMON_UNUSE = "0";
	/** 用户注册 */
	public static final String USER_REGISTER = "0";
	
}
