package com.rlax.framework.support.sms;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 短信发送工具类
 * @author Rlax
 *
 */
public class SmsSenderKit {

	private static SmsSender smsSender = null;
	private static final ConcurrentHashMap<String, SmsSender> map = new ConcurrentHashMap<String, SmsSender>();

	private SmsSenderKit() {}
	
	public static SmsSender use(String smsSenderName) {
		smsSender = get(smsSenderName);
		return smsSender;
	}
	
	public static void add(SmsSender smsSender, String smsSenderName) {
		map.put(smsSenderName, smsSender);
	}
	
	public static SmsSender get(String smsSenderName) {
		return map.get(smsSenderName);
	}
	
	/**
	 * 发送短信
	 * @param phoneNo 手机号码
	 * @param message 短信内容
	 */
	public static void send(String phoneNo, String message) {
		smsSender.send(phoneNo, message);
	}
	
	/**
	 * 批量发送短信
	 * @param phoneNo 手机号码
	 * @param message 短信内容
	 */
	public static void send(List<String> phoneNo, String message) {
		smsSender.send(phoneNo, message);
	}
	
	/**
	 * 根据模版发送短信
	 * @param phoneNo 接收号码
	 * @param signName 签名名称
	 * @param templateCode 模版编码
	 * @param paramString 参数串
	 */
	public static void send(String phoneNo, String signName, String templateCode, Map<String, String> params) {
		smsSender.send(phoneNo, signName, templateCode, params);
	}
	
	/**
	 * 批量根据模版发送短信
	 * @param phoneNo 批量接收号码
	 * @param signName 签名名称
	 * @param templateCode 模版编码
	 * @param paramString 参数串
	 */
	public static void send(List<String> phoneNoList, String signName, String templateCode, Map<String, String> params) {
		smsSender.send(phoneNoList, signName, templateCode, params);
	}
}
