package com.rlax.framework.support.file;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * 文件上传接口
 * @author Rlax
 *
 */
public interface FileManager {

	/** 
	 * 获取文件下载endpoint
	 * @return
	 */
	public String getDownloadEndpoint();
	
	/**
	 * 文件上传
	 * @param file
	 * @param rootPath
	 * @param savePath
	 * @return
	 */
	public FileResult save(File file, String savePath);
	
	/**
	 * 保存二进制文件
	 * @param data 图片二进制信息
	 * @param rootPath 跟路径
	 * @param savePath 保存路径
	 * @return state 状态接口
	 */
	public FileResult save(byte[] data, String savePath);
	
	/**
	 * 保存流文件
	 * @param is 流
	 * @param rootPath 跟路径
	 * @param savePath 保存路径
	 * @return state 状态接口
	 */
	public FileResult save(InputStream is, String savePath);
	
	/**
	 * 获取前缀下的文件信息
	 * @param dirPath 前缀
	 * @return
	 */
	public List<FileInfo> list(String dirPath);
	
	/**
	 * 获取前缀下的文件信息
	 * @return
	 */
	public List<FileInfo> list();
	
	/**
	 * 根据文件key获取私有文件下载地址
	 * @param key 存储文件key
	 * @return
	 */
	public String getDownPathByKey(String key);
}
