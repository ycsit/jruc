package com.rlax.framework.generator;

import javax.sql.DataSource;

import com.jfinal.ext2.plugin.activerecord.generator.ModelGeneratorExt;
import com.jfinal.ext2.plugin.druid.DruidEncryptPlugin;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.Generator;

/**
 * 代码生成工具
 * 
 * @author Rlax
 * 
 */
public class GeneratorModel {

	public static DataSource getDataSource() {

		Prop p = PropKit.use("cfg.txt");
		DruidEncryptPlugin dbPlugin = new DruidEncryptPlugin("jdbc:mysql://" + p.get("db.mysql.url"), p.get("db.mysql.user"), p.get("db.mysql.password"));
		dbPlugin.start();
		return dbPlugin.getDataSource();
	}

	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.crmscb2b.auto.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/crmscb2b/auto/base";
		
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.crmscb2b.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/crmscb2b/model";
		
		BaseModelGenerator baseGe = new BaseModelGenerator(baseModelPackageName, baseModelOutputDir);
		ModelGeneratorExt modelGe = new ModelGeneratorExt(modelPackageName, baseModelPackageName, modelOutputDir);
		Generator ge = new Generator(getDataSource(), baseGe, modelGe);
		ge.setGenerateDataDictionary(true);
		
		// 设置数据库方言
		ge.setDialect(new MysqlDialect());
		// 添加不需要生成的表名
		ge.addExcludedTable("oauth2_client", "oauth2_token", "index_msg");
		// 设置是否在 Model 中生成 dao 对象
		ge.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		ge.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		ge.setRemovedTableNamePrefixes("system_", "t_", "T_");
		// 生成
		ge.generate();
	}
}
