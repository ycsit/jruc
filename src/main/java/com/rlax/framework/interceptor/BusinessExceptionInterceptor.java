package com.rlax.framework.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.ext2.core.ControllerExt;
import com.rlax.framework.exception.BusinessException;

/**
 * 业务异常拦截器
 * @author Rlax
 *
 */
public class BusinessExceptionInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		try {
			inv.invoke();
		} catch (BusinessException e) {
			if (inv.getTarget() instanceof ControllerExt) {
				((ControllerExt)inv.getTarget()).onExceptionError(e);
			}
		}
	}

}
