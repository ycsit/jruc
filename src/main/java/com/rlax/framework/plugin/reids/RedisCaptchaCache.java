package com.rlax.framework.plugin.reids;

import java.util.Set;

import com.jfinal.captcha.Captcha;
import com.jfinal.captcha.ICaptchaCache;
import com.jfinal.plugin.redis.Redis;
import com.rlax.framework.common.CacheKey;

/**
 * 验证码 redis 集群
 * @author Rlax
 *
 */
public class RedisCaptchaCache implements ICaptchaCache {

	/** 过期时间 默认180秒*/
	private int expire_time = 180;
	
	@Override
	public void put(Captcha captcha) {
		RedisCaptcha _captcha = new RedisCaptcha(captcha.getKey(), captcha.getValue(), Captcha.DEFAULT_EXPIRE_TIME);
		Redis.use().setex(CacheKey.CACHE_CAPTCHAR_SESSION + "_" + captcha.getKey(), expire_time, _captcha);
	}

	@Override
	public Captcha get(String key) {
		return Redis.use().get(CacheKey.CACHE_CAPTCHAR_SESSION + "_" + key);
	}

	@Override
	public void remove(String key) {
		Redis.use().del(CacheKey.CACHE_CAPTCHAR_SESSION + "_" + key);
	}

	@Override
	public void removeAll() {
        Set<String> keys = Redis.use().keys(CacheKey.CACHE_CAPTCHAR_SESSION + "_" + "*");
        Redis.use().del(keys);
	}

}
