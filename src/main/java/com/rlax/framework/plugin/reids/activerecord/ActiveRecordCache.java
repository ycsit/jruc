package com.rlax.framework.plugin.reids.activerecord;

import com.jfinal.plugin.activerecord.cache.ICache;
import com.rlax.framework.support.cache.CacheExtKit;

/**
 * activerecord 缓存，封装 redis/ehcache
 * @author Rlax
 *
 */
public class ActiveRecordCache implements ICache {

	@Override
	public <T> T get(String cacheName, Object key) {
		return CacheExtKit.get(cacheName, key);
	}

	@Override
	public void put(String cacheName, Object key, Object value) {
		CacheExtKit.put(cacheName, key, value);
	}

	@Override
	public void remove(String cacheName, Object key) {
		CacheExtKit.remove(cacheName, key);
	}

	@Override
	public void removeAll(String cacheName) {
		CacheExtKit.removeAll(cacheName);
	}

}
