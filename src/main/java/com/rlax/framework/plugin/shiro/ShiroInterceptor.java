package com.rlax.framework.plugin.shiro;

import java.util.List;

import org.apache.shiro.SecurityUtils;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 让 shiro 基于 url 拦截
 * 主要 数据库中也用url 保存权限
 * 
 * @author Rlax
 *
 */
public class ShiroInterceptor implements Interceptor {
	/**
	 * 获取全部 需要控制的权限
	 */
	private static List<String> urls;

	public static void updateUrls() {
		//urls = Res.dao.getUrls();
	}

	public void intercept(Invocation ai) {
		//if (urls == null) urls = Res.dao.getUrls();

		String url = ai.getActionKey();
		boolean flag = SecurityUtils.getSubject() != null && SecurityUtils.getSubject().isPermitted(url);
		try {
			/**
			if (url.contains("delete")) Log.dao.insert(ai.getController(), Log.EVENT_DELETE);
			else if (url.contains("save")) Log.dao.insert(ai.getController(), Log.EVENT_ADD);
			 */
			if (urls.contains(url) && !flag) ai.getController().renderError(403);

		} catch (Exception e) {
			ai.getController().renderError(403);
		}

		ai.invoke();

	}

}
