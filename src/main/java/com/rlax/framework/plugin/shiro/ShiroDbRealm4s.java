package com.rlax.framework.plugin.shiro;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.jfinal.kit.HashKit;
import com.rlax.framework.exception.BusinessException;
import com.rlax.web.model.Res4s;
import com.rlax.web.model.User4s;

/**
 * ShiroDbRealm 为系统登录用
 * @author Rlax
 *
 */
public class ShiroDbRealm4s extends AuthorizingRealm {

	@Override
	public void setCacheManager(CacheManager cacheManager) {
		super.setCacheManager(cacheManager);
		ShiroCacheUtils.setCacheManager(cacheManager);
	}

	/**
	 * 认证回调函数,登录时调用.
	 */
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		MuitiLoginToken token = (MuitiLoginToken) authcToken;
		
		AuthenticationInfo info = null;
		if (token.getLoginType().equals(MuitiLoginToken.USERPASSWORD_MODE)) {
			info = userPasswordMode(token);
		} else if (token.getLoginType().equals(MuitiLoginToken.TOKEN_MODE)) {
			info = tokenMode(token);
		} else {
			throw new BusinessException("not support login type :" + token.getLoginType());
		}
		
		return info;
	}

	/**
	 * token 认证模式
	 * @param token
	 * @return
	 */
	private AuthenticationInfo tokenMode(MuitiLoginToken token) {
		User4s user = User4s.dao.findByName(token.getUsername());
		if (user == null) throw new UnknownAccountException();// 没找到帐号
		if (user.getStatus().equals("2")) throw new LockedAccountException(" USER Freeze");
		if (token.getPassword() == null) throw new IncorrectCredentialsException("token not exist, please set in password attr");
		
		return new SimpleAuthenticationInfo(user.getName(), HashKit.sha1(user.getSalt2()).toUpperCase(), getName());
	}

	/**
	 * 用户名密码认证模式
	 * @param token
	 * @return
	 */
	private AuthenticationInfo userPasswordMode(MuitiLoginToken token) {
		User4s user = User4s.dao.findByName(token.getUsername());

		if (user == null) throw new UnknownAccountException();// 没找到帐号
		if (user.getStatus().equals("2")) throw new LockedAccountException(" USER Freeze");
		return new SimpleAuthenticationInfo(user.getName(), user.getPwd(), ByteSource.Util.bytes(user.getPhone() + user.getSalt2()), getName());
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String loginName = (String) principals.fromRealm(getName()).iterator().next();
		User4s user = User4s.dao.findByName(loginName);

		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			List<Long> roleIds = User4s.dao.findRoleIdByUserID(user.getId());
			
			List<String> idList = new ArrayList<String>();
			for (Long roleId : roleIds) {
				idList.add(roleId.toString());
			}
			
			info.addRoles(idList);
			List<String> urlList = Res4s.dao.findUrlByRoleIdsOnUse(roleIds.toArray(new Long[roleIds.size()]));
			info.addStringPermissions(urlList);
			
			return info;
		} else {
			return null;
		}
	}

	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());

		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();

		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}

}
