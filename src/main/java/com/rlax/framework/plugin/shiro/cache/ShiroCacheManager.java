package com.rlax.framework.plugin.shiro.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * 封装shiro manager 扩展使用 redis/ehcache
 * @author Rlax
 *
 */
public class ShiroCacheManager implements CacheManager {

	@Override
	public <K, V> Cache<K, V> getCache(String name) throws CacheException {
		return new ShiroCache<K, V>(name);
	}

}
