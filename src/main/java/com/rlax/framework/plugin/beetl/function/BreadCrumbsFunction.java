package com.rlax.framework.plugin.beetl.function;

import java.util.ArrayList;
import java.util.List;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.jfinal.kit.StrKit;
import com.rlax.framework.plugin.shiro.ShiroUtils;
import com.rlax.web.model.Res4s;

/**
 * 面包屑导航
 * @author Rlax
 *
 */
public class BreadCrumbsFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		String key = (String) paras[0];
		if (StrKit.isBlank(key)) {
			return null;
		}
		
		String[] keys = key.split("-");
		
		List<Res4s> all = ShiroUtils.getResList();
		List<Res4s> list = new ArrayList<Res4s>();
		
		for (String id : keys) {
			for (Res4s res : all) {
				if (("function_" + res.getId()).equals(id)) {
					list.add(res);
				}
			}
		}
		
		return list;
	}

}
