package com.rlax.framework.plugin.beetl.function;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.rlax.web.model.Data;

/**
 * 根据key value查询 value 对应描述
 * @author Rlax
 *
 */
public class ValueDescFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		String key = (String) paras[0];
		String value = (String) paras[1];
		
		return Data.dao.getCodeDescByCodeAndType(value, key);
	}

}
