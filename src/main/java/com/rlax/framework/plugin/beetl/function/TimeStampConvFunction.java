package com.rlax.framework.plugin.beetl.function;

import java.util.Date;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.jfinal.ext2.kit.DateTimeKit;
import com.jfinal.kit.StrKit;

/**
 * 时间戳转换为字符串
 * @author Rlax
 *
 */
public class TimeStampConvFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		if (paras[0] == null || StrKit.isBlank(paras[0].toString())) {
			return "";
		}
		
		Long timestamp = Long.parseLong(paras[0].toString());
		String spacer = "-";
		
		if (paras.length > 1) {
			spacer = (String) paras[1];
		}
		return DateTimeKit.formatDateToFULL24HRStyle(spacer, new Date(timestamp * 1000));
	}

}
