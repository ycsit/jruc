package com.rlax.framework.plugin.beetl.function;

import java.util.List;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.rlax.framework.plugin.shiro.ShiroUtils;
import com.rlax.web.model.Res4s;
import com.rlax.web.model.Role4s;

/**
 * 获取导航菜单Function
 * @author Rlax
 *
 */
public class MenuFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		List<Role4s> roleList = ShiroUtils.getLoginUserRole4s();
		return Res4s.dao.findMenuByRolesOnUseFromCache(roleList.toArray(new Role4s[roleList.size()]));
	}

}
