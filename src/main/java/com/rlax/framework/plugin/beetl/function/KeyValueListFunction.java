package com.rlax.framework.plugin.beetl.function;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.rlax.web.model.Data;

/**
 * 根据key获取下拉list
 * @author Rlax
 *
 */
public class KeyValueListFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		String key = (String) paras[0];
		return Data.dao.getMapByTypeOnUse(key);
	}

}
