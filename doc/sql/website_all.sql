SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `website_data`
-- 基础数据表
-- ----------------------------
DROP TABLE IF EXISTS `website_data`;
CREATE TABLE `website_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '代码',
  `codeDesc` varchar(200) DEFAULT NULL COMMENT '代码描述',
  `type` varchar(20) DEFAULT NULL COMMENT '类型代码',
  `typeDesc` varchar(200) COMMENT '类型描述',
  `status` varchar(2) COMMENT '状态 0-未生效 1-已生效',
  `orderNo` varchar(255) DEFAULT NULL COMMENT '排序',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,	  
  PRIMARY KEY (`id`),
  KEY (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `website_res`
-- 资源URL表
-- ----------------------------
DROP TABLE IF EXISTS `website_res`;
CREATE TABLE `website_res` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `iconCls` varchar(200) DEFAULT NULL,
  `orderNo` int(11) DEFAULT '1',
  `type` varchar(2) DEFAULT '2' COMMENT '1 功能 2 权限',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,	
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `website_role`
-- 角色表
-- ----------------------------
DROP TABLE IF EXISTS `website_role`;
CREATE TABLE `website_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `orderNo` int(11) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` int(11) DEFAULT '0',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `website_user`
-- 网站用户表
-- ----------------------------
DROP TABLE IF EXISTS `website_user`;
CREATE TABLE `website_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(20) NOT NULL COMMENT '用户编号',
  `name` varchar(55) DEFAULT NULL COMMENT '用户名称-登录用户名',
  `pwd` varchar(255) DEFAULT NULL COMMENT '密码',
  `status` varchar(2) DEFAULT '1' COMMENT '#1 正常 2.封号',
  `icon` varchar(255) DEFAULT 'images/guest.jpg',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `createdate` datetime DEFAULT NULL COMMENT '创建日期',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `salt2` varchar(55) DEFAULT NULL COMMENT 'salt',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,	  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `website_weixin_user`
-- 微信用户表
-- ----------------------------
DROP TABLE IF EXISTS `website_weixin_user`;
CREATE TABLE `website_weixin_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(40) NOT NULL COMMENT '用户编码',
  `openId` varchar(40) NOT NULL,
  `nickName` varchar(100) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `headImgUrl` varchar(200) DEFAULT NULL,
  `privilege` varchar(200) DEFAULT NULL,
  `unionid` varchar(40) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `status` varchar(2) NOT NULL,
  `lastUpdAcct` varchar(20) NOT NULL,
  `lastUpdTime` varchar(20) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `website_page`
-- 页面表
-- ----------------------------
DROP TABLE IF EXISTS `website_page`;
CREATE TABLE `website_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(40) NOT NULL COMMENT '用户编码',
  `name` varchar(40) NOT NULL COMMENT '页面名称',
  `desc` varchar(200) DEFAULT NULL COMMENT '页面描述',
  `content` text DEFAULT NULL COMMENT '页面内容',
  `preContent` varchar(200) DEFAULT NULL COMMENT '预览内容',
  `createDate` datetime NOT NULL,
  `status` varchar(2) NOT NULL COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) NOT NULL,
  `lastUpdTime` varchar(20) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
